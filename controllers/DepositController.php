<?php

namespace app\controllers;

use app\models\Client;
use app\models\DepositHistory;
use Yii;
use app\models\Deposit;
use app\models\search\Deposit as DepositSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DepositController implements the CRUD actions for Deposit model.
 */
class DepositController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Deposit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Deposit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Deposit();
        $model->client_id = Yii::$app->request->get('client_id');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Deposit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionTest()
    {
        $depositsQ = Deposit::find();

        /** @var Deposit $deposit */
        foreach ($depositsQ->each() as $deposit) {
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $deposit->accruePercent();
                $deposit->accrueCommission();
                $transaction->commit();
            } catch (ErrorException $e) {
                $transaction->rollBack();
            }
        }

        return $this->render('test');
    }

    public function actionReport()
    {
        $percents = DepositHistory::find()
            ->select(['amount'=>'SUM(amount)', 'created_at_formatted'=>'DATE_FORMAT(FROM_UNIXTIME(`created_at`), "%Y-%m")'])
            ->where(['action' => DepositHistory::ACTION_ACCRUE])
            ->groupBy('created_at_formatted')
            ->orderBy(['created_at_formatted' => SORT_DESC])
            ->asArray()->all();

        $commissions = DepositHistory::find()
            ->select(['amount'=>'SUM(amount)', 'created_at_formatted'=>'DATE_FORMAT(FROM_UNIXTIME(`created_at`), "%Y-%m")'])
            ->where(['action' => DepositHistory::ACTION_COMMISSION])
            ->groupBy('created_at_formatted')
            ->orderBy(['created_at_formatted' => SORT_DESC])
            ->asArray()->all();

        $age18 = date('Y-m-d', strtotime('-18years'));
        $age25 = date('Y-m-d', strtotime('-25years'));
        $age50 = date('Y-m-d', strtotime('-50years'));

        $ageStatistics18_25 = Client::find()
            ->from('{{%client}} c')
            ->select(['averageVal' => 'AVG(d.amount)'])
            ->where(['and', ['>', 'birthdate', $age25], ['<=', 'birthdate', $age18]])
            ->leftJoin('{{%deposit}} d', 'c.id = d.client_id')
            ->scalar();

        $ageStatistics25_50 = Client::find()
            ->from('{{%client}} c')
            ->select(['averageVal' => 'AVG(d.amount)'])
            ->where(['and', ['>', 'birthdate', $age50], ['<=', 'birthdate', $age25]])
            ->leftJoin('{{%deposit}} d', 'c.id = d.client_id')
            ->scalar();

        $ageStatistics50 = Client::find()
            ->from('{{%client}} c')
            ->select(['averageVal' => 'AVG(d.amount)'])
            ->where(['<=', 'birthdate', $age50])
            ->leftJoin('{{%deposit}} d', 'c.id = d.client_id')
            ->scalar();
        return $this->render('report', [
            'percents' => $percents,
            'commissions' => $commissions,
            'ageStatistics18_25' => $ageStatistics18_25,
            'ageStatistics25_50' => $ageStatistics25_50,
            'ageStatistics50' => $ageStatistics50,
            ]);
    }

    /**
     * Finds the Deposit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deposit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deposit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
