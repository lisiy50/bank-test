-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 20, 2018 at 12:51 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `iin` bigint(10) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `secondname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `iin`, `name`, `secondname`, `sex`, `birthdate`) VALUES
(1, 2222222222, 'Maria', 'Hizhnyak', 2, '2000-02-05'),
(2, 3333333333, 'Nikolay', 'Pasechnik', 1, '1987-11-06');

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `percent` float NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deposit`
--

INSERT INTO `deposit` (`id`, `client_id`, `amount`, `percent`, `created_at`, `updated_at`) VALUES
(1, 1, 7083.86, 12, 1520429364, 1529498772),
(2, 1, 180.695, 1, 1529035840, 1529498772),
(3, 2, 4139.71, 30, 1529408360, 1529498772);

-- --------------------------------------------------------

--
-- Table structure for table `deposit_history`
--

CREATE TABLE `deposit_history` (
  `id` int(11) NOT NULL,
  `deposit_id` int(11) NOT NULL,
  `action` tinyint(1) NOT NULL,
  `amount` float NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deposit_history`
--

INSERT INTO `deposit_history` (`id`, `deposit_id`, `action`, `amount`, `created_at`) VALUES
(1, 1, 1, 120, 1529430817),
(2, 1, 1, 134.4, 1529430920),
(3, 1, 1, 150.528, 1529430921),
(4, 1, 1, 168.592, 1529430922),
(5, 1, 2, 88.7465, 1529438558),
(6, 2, 2, 50, 1529438558),
(7, 3, 2, 50, 1529438558),
(8, 1, 2, 83.4218, 1529438607),
(9, 2, 2, 50, 1529438607),
(10, 3, 2, 50, 1529438607),
(11, 1, 2, 78.4163, 1529438612),
(12, 2, 2, 50, 1529438612),
(13, 3, 2, 50, 1529438612),
(14, 1, 1, 175.653, 1529438659),
(15, 2, 1, 2.9694, 1529438659),
(16, 3, 1, 195, 1529438659),
(17, 1, 1, 196.731, 1529438665),
(18, 2, 1, 2.99909, 1529438665),
(19, 3, 1, 253.5, 1529438665),
(20, 1, 1, 220.338, 1529438673),
(21, 2, 1, 3.02908, 1529438673),
(22, 3, 1, 329.55, 1529438673),
(23, 1, 2, 0.0000000720331, 1529439143),
(24, 2, 2, 0.0000000326921, 1529439144),
(25, 1, 2, 1.05871, 1529439320),
(26, 1, 2, 1.05809, 1529439336),
(27, 1, 2, 1.05748, 1529439404),
(28, 2, 2, 10, 1529439404),
(29, 1, 2, 1.05687, 1529439421),
(30, 2, 2, 10, 1529439421),
(31, 1, 2, 1.05626, 1529440365),
(32, 2, 2, 10, 1529440366),
(33, 1, 1, 246.066, 1529441654),
(34, 1, 2, 1.18233, 1529441654),
(35, 2, 1, 2.75637, 1529441654),
(36, 2, 2, 10, 1529441654),
(38, 1, 1, 275.435, 1529441740),
(39, 1, 2, 1.32344, 1529441740),
(40, 2, 1, 2.68293, 1529441740),
(41, 2, 2, 10, 1529441740),
(43, 1, 1, 308.31, 1529441753),
(44, 1, 2, 1.4814, 1529441753),
(45, 2, 1, 2.60876, 1529441753),
(46, 2, 2, 10, 1529441753),
(49, 1, 1, 345.107, 1529441796),
(50, 1, 2, 1.65821, 1529441796),
(51, 2, 1, 2.53385, 1529441796),
(52, 2, 2, 10, 1529441796),
(54, 1, 1, 386.297, 1529441810),
(55, 1, 2, 1.85612, 1529441810),
(56, 2, 1, 2.45819, 1529441810),
(57, 2, 2, 10, 1529441810),
(60, 1, 1, 432.402, 1529441838),
(61, 1, 2, 2.07766, 1529441838),
(62, 2, 1, 2.38177, 1529441838),
(63, 2, 2, 10, 1529441838),
(65, 1, 1, 484.011, 1529441869),
(66, 1, 2, 2.32563, 1529441869),
(67, 2, 1, 2.30459, 1529441870),
(68, 2, 2, 10, 1529441870),
(69, 3, 1, 428.415, 1529441870),
(70, 3, 2, 0, 1529441870),
(71, 1, 1, 541.78, 1529441880),
(72, 1, 2, 2.60321, 1529441880),
(73, 2, 1, 2.22664, 1529441880),
(74, 2, 2, 10, 1529441880),
(75, 3, 1, 556.94, 1529441880),
(76, 3, 2, 0, 1529441880),
(77, 1, 1, 606.444, 1529487094),
(78, 1, 2, 2.88618, 1529487094),
(79, 2, 1, 2.14791, 1529487094),
(80, 2, 2, 10, 1529487094),
(81, 3, 1, 724.019, 1529487094),
(82, 3, 2, 136.116, 1529487094),
(83, 1, 1, 678.829, 1529494745),
(84, 1, 2, 3.23067, 1529494745),
(85, 2, 1, 2.06839, 1529494745),
(86, 2, 2, 10, 1529494745),
(87, 3, 1, 884.75, 1529494745),
(88, 3, 2, 166.333, 1529494745),
(89, 1, 1, 759.853, 1529495491),
(90, 1, 2, 3.61628, 1529495491),
(91, 2, 1, 1.98807, 1529495491),
(92, 2, 2, 10, 1529495491),
(93, 3, 1, 1081.17, 1529495491),
(94, 3, 2, 203.259, 1529495491),
(95, 1, 1, 850.549, 1529498772),
(96, 1, 2, 4.04792, 1529498772),
(97, 2, 1, 1.90695, 1529498772),
(98, 2, 2, 10, 1529498772),
(99, 3, 1, 1321.18, 1529498772),
(100, 3, 2, 248.382, 1529498772);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1529425168),
('m180619_162214_create_client_table', 1529426891),
('m180619_164938_create_deposit_table', 1529429179),
('m180619_170406_create_deposit_history_table', 1529429179),
('m180619_191455_alter_deposit_table', 1529436740);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `iin` (`iin`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deposit-client_id` (`client_id`),
  ADD KEY `deposit-created_at` (`created_at`),
  ADD KEY `history-deposit_id` (`client_id`),
  ADD KEY `history-created_at` (`created_at`);

--
-- Indexes for table `deposit_history`
--
ALTER TABLE `deposit_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-deposit_history-deposit_id` (`deposit_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `deposit_history`
--
ALTER TABLE `deposit_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deposit`
--
ALTER TABLE `deposit`
  ADD CONSTRAINT `fk-deposit-client_id` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`);

--
-- Constraints for table `deposit_history`
--
ALTER TABLE `deposit_history`
  ADD CONSTRAINT `fk-deposit_history-deposit_id` FOREIGN KEY (`deposit_id`) REFERENCES `deposit` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
