<?php
namespace app\commands;

use app\models\Deposit;
use yii\base\ErrorException;
use yii\console\Controller;

/**
 * Class DepositController
 * @package app\commands
 */
class DepositController extends Controller
{
    /**
     * будем считать, что метод будет запускаться раз в день по крону
     * здесь есть проблема, проверка можно ли начислить проценты идет от поля updated_at,
     * которое обновляется при любом редактировании записи, будем считать, что редактирования не будет.
     */
    public function actionAccruePercent()
    {
        $depositsQ = Deposit::find()->andWhere(['>', 'updated_at', strtotime('+1day')]);
        if (date('d') === date('t')) {
            $depositsQ->andWhere(['>=', 'DATE_FORMAT(FROM_UNIXTIME(`created_at`), "%d")', date('d')]);
        } else {
            $depositsQ->andWhere(['DATE_FORMAT(FROM_UNIXTIME(`created_at`), "%d")' => date('d')]);
        }

        /** @var Deposit $deposit */
        foreach ($depositsQ->each() as $deposit) {
            try {
                $deposit->accruePercent();
            } catch (ErrorException $e) {
                // TODO: do something
            }
        }
    }

    /**
     * будем считать, что этот метод будет запускаться рез в месяц первого числа по крону
     */
    public function actionCommission()
    {
        $depositsQ = Deposit::find()->andWhere(['>', 'created_at', strtotime('+1day')]);

        /** @var Deposit $deposit */
        foreach ($depositsQ->each() as $deposit) {
            try {
                $deposit->accrueCommission();
            } catch (ErrorException $e) {
                // TODO: do something
            }
        }
    }
}
