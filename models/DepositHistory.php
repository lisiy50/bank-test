<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%deposit_history}}".
 *
 * @property int $id
 * @property int $deposit_id
 * @property int $action
 * @property double $amount
 * @property int $created_at
 *
 * @property Deposit $deposit
 */
class DepositHistory extends \yii\db\ActiveRecord
{
    const ACTION_ACCRUE = 1,
        ACTION_COMMISSION=2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%deposit_history}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deposit_id', 'action', 'amount'], 'required'],
            [['deposit_id', 'action', 'created_at'], 'integer'],
            [['amount'], 'number'],
            [['deposit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deposit::className(), 'targetAttribute' => ['deposit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deposit_id' => 'Deposit ID',
            'action' => 'Action',
            'amount' => 'Amount',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeposit()
    {
        return $this->hasOne(Deposit::className(), ['id' => 'deposit_id']);
    }
}
