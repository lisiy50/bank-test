<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%client}}".
 *
 * @property int $id
 * @property int $iin
 * @property string $name
 * @property string $secondname
 * @property int $sex
 * @property string $birthdate
 *
 * @property array $sexList
 * @property Deposit[] $deposits
 */
class Client extends \yii\db\ActiveRecord
{
    const SEX_MALE = 1,
        SEX_FEMALE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iin', 'name', 'secondname', 'sex', 'birthdate'], 'required'],
            [['iin', 'sex'], 'integer'],
            [['birthdate'], 'safe'],
            [['name', 'secondname'], 'string', 'max' => 64],
            [['iin'], 'unique'],
            [['sex'], 'in', 'range' => [self::SEX_MALE, self::SEX_FEMALE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iin' => 'Iin',
            'name' => 'Name',
            'secondname' => 'Secondname',
            'sex' => 'Sex',
            'birthdate' => 'Birthdate',
        ];
    }

    public function getSexList()
    {
        return [
            self::SEX_MALE => 'Male',
            self::SEX_FEMALE => 'Female',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeposits()
    {
        return $this->hasMany(Deposit::className(), ['client_id' => 'id']);
    }
}
