<?php

namespace app\models;

use Yii;
use yii\base\ErrorException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%deposit}}".
 *
 * @property int $id
 * @property int $client_id
 * @property double $amount
 * @property double $percent
 * @property int $created_at
 *
 * @property Client $client
 * @property DepositHistory[] $depositHistories
 */
class Deposit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%deposit}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'amount', 'percent'], 'required'],
            [['client_id'], 'integer'],
            [['amount', 'percent'], 'number'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'amount' => 'Amount',
            'percent' => 'Percent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepositHistories()
    {
        return $this->hasMany(DepositHistory::className(), ['deposit_id' => 'id']);
    }

    public function getAccrueRate()
    {
        return $this->amount * $this->percent / 100;
    }

    public function getCommissionRate()
    {
        if ($this->amount < 1000) {
            $rate = 0.05;
        } elseif ($this->amount < 10000) {
            $rate = 0.06;
        } else {
            $rate = 0.07;
        }
        return $rate;
    }

    public function getCommission()
    {
        $commission = $this->amount * $this->getCommissionRate();
        if ($commission < 50) {
            $commission = 50;
        } elseif ($commission > 5000) {
            $commission = 5000;
        }

        if (strtotime(date('Y-m-d') . ' +1month') > time()) {
            $diff = (int)round((time() - $this->created_at) / (60 * 60 * 24));
            if ($diff === 0) {
                $commission = 0;
            } else {
                $commission = $commission / $diff;
            }
        }

        return $commission;
    }

    public function accruePercent()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $this->amount += $this->getAccrueRate();
        if  ($this->save() && $this->addHistory(DepositHistory::ACTION_ACCRUE, $this->getAccrueRate())) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            throw new ErrorException('Amount does not change.');
        }
    }

    public function accrueCommission()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $this->amount -= $this->getCommission();
        if  ($this->save() && $this->addHistory(DepositHistory::ACTION_COMMISSION, $this->getCommission())) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            throw new ErrorException('Amount does not change.');
        }
    }

    /**
     * @param $action
     * @param float $amount
     * @return bool
     * @throws ErrorException
     */
    public function addHistory($action, float $amount)
    {
        if ($this->isNewRecord) {
            throw new ErrorException('You should save deposit before.');
        }
        $history = new DepositHistory();
        $history->deposit_id = $this->id;
        $history->action = $action;
        $history->amount = $amount;
        return $history->save();
    }
}
