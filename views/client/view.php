<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'iin',
            'name',
            'secondname',
            'sex',
            'birthdate',
        ],
    ]) ?>


	<p>
        <?= Html::a('Create Deposit', ['deposit/create', 'client_id' => $model->id], ['class' => 'btn btn-success']) ?>
	</p>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
			'allModels' => $model->deposits,
			'sort' => [
                'attributes' => ['amount', 'percent', 'created_at'],
			],
		]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'amount',
            'percent',
            'created_at:date',

			[
				'content' => function($data){
    				return Html::a(Html::tag('span', '', ['class' => "glyphicon glyphicon-eye-open"]), ['deposit/view', 'id' => $data->id]);
				},
				'format' => 'html',
			],
        ],
    ]); ?>

</div>
