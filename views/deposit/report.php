<?php
/**
 * @var \yii\web\View $this
 * @var array $percents
 * @var array $commission
 * @var float $ageStatistics18_25
 * @var float $ageStatistics25_50
 * @var float $ageStatistics50
 */
?>

<table class="table table-bordered table-striped table-hover">
    <tr>
        <th colspan="2">
            Начислено процентов пользователям (убыток банка)
        </th>
    </tr>
    <tr>
        <th>
            месяц
        </th>
        <th>
            сумма
        </th>
    </tr>
    <?php $percentsSum = 0; ?>
    <?php foreach ($percents as $item): ?>
        <tr>
            <td><?= $item['created_at_formatted']?></td>
            <td><?= $item['amount']?></td>
        </tr>
    <?php $percentsSum += $item['amount']; ?>
    <?php endforeach; ?>

    <tr>
        <th>
            итого:
        </th>
        <th>
            <?= $percentsSum; ?>
        </th>
    </tr>
</table>


<table class="table table-bordered table-striped table-hover">
    <tr>
        <th colspan="2">
            Снятия комисии (прибыль банка)
        </th>
    </tr>
    <tr>
        <th>
            месяц
        </th>
        <th>
            сумма
        </th>
    </tr>
    <?php $commissionsSum = 0; ?>
    <?php foreach ($commissions as $item): ?>
        <tr>
            <td><?= $item['created_at_formatted']?></td>
            <td><?= $item['amount']?></td>
        </tr>
    <?php $commissionsSum += $item['amount']; ?>
    <?php endforeach; ?>

    <tr>
        <th>
            итого:
        </th>
        <th>
            <?= $commissionsSum; ?>
        </th>
    </tr>
</table>


<table class="table table-bordered table-striped table-hover">
    <tr>
        <th colspan="3">
            Средний размер депозита
        </th>
    </tr>
    <tr>
        <th>
            18 - 25
        </th>
        <th>
            25 - 50
        </th>
        <th>
            50+
        </th>
    </tr>
    <tr>
        <td><?= $ageStatistics18_25; ?></td>
        <td><?= $ageStatistics25_50; ?></td>
        <td><?= $ageStatistics50; ?></td>
    </tr>
</table>
