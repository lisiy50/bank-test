<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Deposit */

$this->title = 'Create Deposit';
$this->params['breadcrumbs'][] = ['label' => $model->client->name, 'url' => ['client/view', 'id' => $model->client->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deposit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
