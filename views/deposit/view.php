<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Deposit */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => $model->client->name, 'url' => ['client/view', 'id' => $model->client->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deposit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'client_id',
            'amount',
            'percent',
            'created_at:datetime',
        ],
    ]) ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ArrayDataProvider([
            'allModels' => $model->depositHistories,
            'sort' => [
                'attributes' => ['amount', 'percent', 'created_at'],
            ],
			'pagination' => [
				'pageSize' => 1000,
			]
        ]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
				'attribute' => 'amount',
				'value' => function($data) {
					if ($data->action == \app\models\DepositHistory::ACTION_ACCRUE) {
						return $data->amount;
					} elseif ($data->action == \app\models\DepositHistory::ACTION_COMMISSION) {
                        return '-' . $data->amount;
					}
				}
			],
            'created_at:datetime',
        ],
    ]); ?>

</div>
