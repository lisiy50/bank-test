<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deposit_history`.
 */
class m180619_170406_create_deposit_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%deposit_history}}', [
            'id' => $this->primaryKey(),
            'deposit_id' => $this->integer()->notNull(),
            'action' => $this->tinyInteger(1)->notNull(),
            'amount' => $this->float()->notNull(),
            'created_at' => $this->integer(),
        ]);
        $this->createIndex('history-deposit_id', '{{%deposit}}', 'client_id');
        $this->createIndex('history-created_at', '{{%deposit}}', 'created_at');

        $this->addForeignKey(
            'fk-deposit_history-deposit_id',
            '{{%deposit_history}}',
            'deposit_id',
            '{{%deposit}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('deposit_history');
    }
}
