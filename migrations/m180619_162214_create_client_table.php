<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m180619_162214_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client}}', [
            'id' => $this->primaryKey(),
            'iin' => $this->bigInteger(10)->notNull()->unique(),
            'name' => $this->string(64)->notNull(),
            'secondname' => $this->string(64)->notNull(),
            'sex' => $this->tinyInteger(1)->notNull(),
            'birthdate' => $this->date()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client');
    }
}
