<?php

use yii\db\Migration;

/**
 * Class m180619_191455_alter_deposit_table
 */
class m180619_191455_alter_deposit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%deposit}}', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%deposit}}', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180619_191455_alter_deposit_table cannot be reverted.\n";

        return false;
    }
    */
}
