<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deposit`.
 */
class m180619_164938_create_deposit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%deposit}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'amount' => $this->float()->notNull(),
            'percent' => $this->float()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('deposit-client_id', '{{%deposit}}', 'client_id');
        $this->createIndex('deposit-created_at', '{{%deposit}}', 'created_at');

        $this->addForeignKey(
            'fk-deposit-client_id',
            '{{%deposit}}',
            'client_id',
            '{{%client}}',
            'id'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('deposit');
    }
}
